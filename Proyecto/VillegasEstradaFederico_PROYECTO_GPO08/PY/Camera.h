#pragma once

#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT, 
	Q, E,
	Z, C
};

const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 20.0f;
const GLfloat SENSITIVTY = 0.15f;
const GLfloat ZOOM = 45.0f;

class Camera {
public:

	Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH) : front(glm::vec3(0.0f, 0.0f, -1.0f)), movementSpeed(SPEED), mouseSensitivity(SENSITIVTY), zoom(ZOOM) {
		this->position = position;
		this->worldUp = up;
		this->yaw = -120;
		this->pitch = pitch;
		this->updateCameraVectors();
	}

	Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) : front(glm::vec3(0.0f, 0.0f, -1.0f)), movementSpeed(SPEED), mouseSensitivity(SENSITIVTY), zoom(ZOOM) {
		this->position = glm::vec3(posX, posY, posZ);
		this->worldUp = glm::vec3(upX, upY, upZ);
		this->yaw = yaw;
		this->pitch = pitch;
		this->updateCameraVectors();
	}

	glm::mat4 GetViewMatrix() {
		return glm::lookAt(this->position, this->position + this->front, this->up);
	}

	void ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime) {
		GLfloat velocity = this -> movementSpeed * deltaTime;

		if (direction == Q) {
			this->yaw -= 0.1;
			this->updateCameraVectors();
		}

		if (direction == E) {
			this->yaw += 0.1;
			this->updateCameraVectors();
		}

		if (direction == Z) {
			this->pitch += 0.1;
			this->updateCameraVectors();
		}

		if (direction == C) {
			this->pitch -= 0.1;
			this->updateCameraVectors();
		}

		if (direction == FORWARD) {

			if (this->position.y >= 2 || this-> front.y >= 0) {
			
				if ((this->position.x <= 14 && this-> position.x >= -10) && (this->position.z <= 0 && this->position.z >= -58.0) && (this->position.y <= 12.0)) {
					
					if ((this -> front.z >= 0 || this->position.z > -57.5) && (this->position.y < 11.5 || this->front.y <= 0) && (this->front.x <= 0 || this->position.x < 13.5) && (this->front.x >= 0 || this->position.x > -9.0)) {
						this -> position += this -> front * velocity;
						printf("FORWARD \n");
					}	

				} else {
					this->position += this->front * velocity;
				}
				
			}
		}

		if (direction == BACKWARD) {

			if (this->position.y >= 2 || this->front.y <= 0) {

				if ((this->position.x <= 14 && this->position.x >= -10) && (this->position.z <= 0 && this->position.z >= -58.0) && (this->position.y <= 12.0)) {

					if ((this->front.z <= 0 || this->position.z > -57.5) && (this->position.y < 11.5 || this->front.y >= 0) && (this->front.x >= 0 || this->position.x < 13.5) && (this->front.x <= 0 || this->position.x > -9.0)) {
						this->position -= this->front * velocity;
						printf("BACKWARD \n");
					}
				} else {
					this->position -= this->front * velocity;
				}
				
			}
		}

		if (direction == LEFT) {

			if ((this->position.x <= 14 && this->position.x >= -10) && (this->position.z <= 0 && this->position.z >= -58.0) && (this->position.y <= 12.0)) {
				if ((this->position.z > -57.5 || this->front.x <= 0) && (this->front.x <= 0 || this->position.x < 13.5) && (this->front.x >= 0 || this->position.x > -9.0)) {
					this->position -= this->right * velocity;
					printf("LEFT \n");
				}
			}
			else {
				this->position -= this->right * velocity;
			}
			
		}

		if (direction == RIGHT) {

			if ((this->position.x <= 14 && this->position.x >= -10) && (this->position.z <= 0 && this->position.z >= -58.0) && (this->position.y <= 12.0)) {
				if ((this->position.z > -57.5 || this->front.x >= 0) && (this->front.x >= 0 || this->position.x < 13.5) && (this->front.x <= 0 || this->position.x > -9.0)) {
					this->position += this->right * velocity;
					printf("RIGHT \n");
				}
			}
			else {
				this->position += this->right * velocity;
			}
		
		}
	}

	void ProcessMouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constrainPitch = true) {
		xOffset *= this -> mouseSensitivity;
		yOffset *= this -> mouseSensitivity;

		this->yaw += xOffset;
		this->pitch += yOffset;

		if (constrainPitch) {
			if (this->pitch > 89.0f) {
				this->pitch = 89.0f;
			}

			if (this->pitch < -89.0f) {
				this->pitch = -89.0f;
			}
		}

		this->updateCameraVectors();
	}

	void ProcessMouseScroll(GLfloat yOffset) { }

	GLfloat GetZoom() {
		return this->zoom;
	}

	glm::vec3 GetPosition() {
		return this->position;
	}

	glm::vec3 GetFront() {
		return this-> front;
	}

private:
	
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;

	GLfloat yaw;
	GLfloat pitch;

	GLfloat movementSpeed;
	GLfloat mouseSensitivity;
	GLfloat zoom;

	void updateCameraVectors() {
		glm::vec3 front;
		front.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
		front.y = sin(glm::radians(this->pitch));
		front.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
		this->front = glm::normalize(front);
		this->right = glm::normalize(glm::cross(this->front, this->worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		this->up = glm::normalize(glm::cross(this->right, this->front));
	}
};